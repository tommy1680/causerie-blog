---
title: 關於
date: '2021-08-23'
categories:
 - 關於
 - 
tags:
 - About
publish: true 
---

## 一位旅館人熱愛資訊科技踏足前端技術的世界

2018〡台北西華飯店 服務

2021〡進修網頁技術

本站運用 VuePress＋Gitlab Pages＋Gitlab CI 建置

***

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="創用 CC 授權條款" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />本站採<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">創用 CC 姓名標示-非商業性-相同方式分享 4.0 國際 授權條款</a>授權