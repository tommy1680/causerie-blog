const themeConfig = require('./configs/themeConfig'); // 引入主題配置,裡面包裹頭部 nav,及側邊欄,與其他配置
const plugins = require('./configs/plugin'); // 引入插件配置
module.exports = {
  locales: {
    '/': {
      lang: 'zh-TW'
    }
  },
  title: "Tommy 168 隨筆",
  description: '不要急，專心致志做好一件事。',
  base: '/causerie-blog/', 
  dest: 'public',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    ['meta', { name: 'viewport', content: 'width=device-width,initial-scale=1,user-scalable=yes' }]
  ],
  theme: 'reco', 
  themeConfig, // 引入主題配置文件,便於集中管理配置
  plugins, // 外部插件配置
  markdown: {
    lineNumbers: true
  },
  output: {
    pathinfo: false,
  },
  optimization: {
    runtimeChunk: true,
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false
  }
};