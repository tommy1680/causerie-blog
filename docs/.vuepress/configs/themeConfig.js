const navConfig = require('./navConfig'); // 引入導覽列配置,透過配置進行管理配置,nav.js後缀名可以省略
const themeconfig = {
    noFoundPageByTencent: false,
    mode: 'dark',
    modePicker: false,
    nav: navConfig, //導覽列配置
    sidebar: {
        '/docs/theme-reco/': [
          '',
          'theme',
          'plugin',
          'api'
        ]
      },  
      type: 'blog',
      blogConfig: {
        category: {
          location: 2, 
          text: 'Category'
        },
        socialLinks: [
        { icon: 'reco-github', link: 'https://github.com/tommy1680' },
        { icon: 'reco-twitter', link: 'https://twitter.com/tommy16866?t=DVqViGXZjE8A4f6etJNyoA&s=09' },
      ]
      },
      lastUpdated: '上次更新',
      author: 'Tommy 168',
      authorAvatar: '/avatar.png',
      record: 'CC BY-NC-SA 授權',
      startYear: '2021'
    };
    
module.exports = themeconfig; // 輸出主題配置