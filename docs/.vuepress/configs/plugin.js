const plugins = [
//插件配置區
['img-lazy']
[
    '@vuepress/last-updated',
    {
        transformer: (timestamp, lang) => {
            // 不要忘了安装 moment
            const moment = require('moment')
            moment.locale(lang)
            return moment(timestamp).fromNow()
        }
    }
]

];
module.exports = plugins; // 輸出