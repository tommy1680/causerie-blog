---
title: Discord軟體 運用於教學環境 懶人包
date: '2021-12-08'
categories:
 - 教學
tags:
 - Discord
publish: true
---

## 學生使用篇

1. 先註冊 Discord [帳號](https://discord.com/register)
   
   使用者名稱不能使用有關個資，如：手機號、生日、真名
   
   **<mark>※請使用暱稱</mark>**
   
   <img title="註冊頁面" src="https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/1.png" alt="圖1" width="306" data-align="inline">

2. [登入帳號](https://discord.com/) 

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/2.png "登入頁面")

下圖為介面說明

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/3.png "使用畫面1")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/4.png "使用畫面2")

3. 登入後需先帳號驗證及資安設定，才能正常使用。請按圖設定，有需要時再打開

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/5.png "資安設定1")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/6.jpg "資安設定2")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/7.jpg "資安設定3")

4. 加入上課群組，連結老師會放在學校學習平台，按加入就自動加入

連結範例: `https://discord.gg/xxxxx` 下圖為邀請示意圖

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/8.png "邀請畫面")

5. 入群後<mark>必要做的事</mark> ，變更群組暱稱，以方便老師辨識

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/9.png "暱稱變更1")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/10.png "暱稱變更2")

6. 按照各群組規則使用

---

續篇-[教師篇](03%20Discord%E8%BB%9F%E9%AB%94%20%E9%81%8B%E7%94%A8%E6%96%BC%E6%95%99%E5%AD%B8%E7%92%B0%E5%A2%83%20%E6%87%B6%E4%BA%BA%E5%8C%85-%E7%BA%8C.md)