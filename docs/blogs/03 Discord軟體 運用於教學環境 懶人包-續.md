---
title: Discord軟體 運用於教學環境 懶人包-續
date: '2021-12-10'
categories:
 - 教學
tags:
 - Discord
publish: true
---

## 教師使用

老師一樣先註冊帳號；如不會註冊請先看上篇 [學生篇](02%20Discord%E8%BB%9F%E9%AB%94%20%E9%81%8B%E7%94%A8%E6%95%99%E5%AD%B8%E7%92%B0%E5%A2%83%20%E6%87%B6%E4%BA%BA%E5%8C%85.md)，再看本篇。

為何說是懶人包，有教學群組模板，詳細相關權限都已設好，會建立與分享群組連結及開視訊畫面教學就行，開箱即用。

### 👉 [ **教學群組模板**](https://discord.new/WeWHBvQ9v22D)  老師同為群組管理員權限很大

模板已更新

模板說明如下 圖示

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/11.png "模板說明1")

簽到處: 會出現學生降落訊息

上課須知頻道:  可用 <mark>@everyone</mark>  群發訊息

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/12.png "模板說明2")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/13.png "模板說明3")

建立好群組，去分享群組連結

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/14.png "模板說明4")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/15.png "模板說明5")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/16.png "模板說明6")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/17.png "模板說明8")

點完複製連結，去公告給學生入群

```
連結範例: `https://discord.gg/xxxxx`
```

<mark>視訊教學</mark>

***原本設在下方雲教室移動至上方跟 Google Meet 教育版頻道一起*** 

Google Meet 使用 Classroom 裡的 google meet 連結

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/18.png "視訊教學1")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/19.png "視訊教學2")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/20.png "視訊教學3")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/21.png "視訊教學4")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/22.png "視訊教學5")
![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/23.png "視訊教學6")

以上懶人包開箱即用說明

---

<mark>進階說明</mark>

如有老師想客製化建立新頻道及刪除頻道，請照圖示操作

<mark>重點</mark>

＊＊教材區已設為學生只能讀看內容不能寫內容

＊＊解惑區已設為老師與學生都能讀及寫

建立用複製頻道最保險

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/24.png "建立頻道1")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/25.png "建立頻道2")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/26.png "建立頻道3")

<mark>刪除頻道</mark> 相關操作請慎重小心，注意不可逆

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/27.png "刪除頻道1")

![img](https://cdn.jsdelivr.net/gh/tommy1680/cdn@main/blog-img1/28.png "刪除頻道2")

### End